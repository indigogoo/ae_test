const autoprefixer = require('autoprefixer');

module.exports = {
  entry: {
    app: './client/app.js'
  },
  output: {
    filename: '[name].entry.js',
    publicPath: 'http://0.0.0.0:3030/assets',
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
      },
      {
        test:   /\.css$/,
        loader: "style-loader!css-loader?modules!postcss-loader"
      }
    ],
  },
  externals: {
    'react': 'React',
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  postcss: function () {
    return [autoprefixer];
  }
}

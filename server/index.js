const Hapi = require('hapi')
const debug = require('debug')('server')
const Boom = require('boom');
const Joi = require('joi');
const async = require('async');
const fetch = require('node-fetch');

const profile = process.env.NODE_ENV || 'development'
const runtime = {
  profile,
  config: require('../config/server.config.' + profile + '.js'),
};

const server = new Hapi.Server()
server.connection({ port: runtime.config.port })

const mongodbPlugin = {
  register: require('hapi-mongodb'),
  options: runtime.config.dbOpts
};

server.register([require('inert'), mongodbPlugin], function (err) {
  if (err) {
      throw err;
  }

  const reactPath = '/node_modules/react/dist/react-with-addons.js';
  server.route({
    method: 'GET',
    path: reactPath,
    handler: {
      file: __dirname + '/..' + reactPath
    }
  });

  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
      directory: {
        path: __dirname + '/../client/',
        index: true
      }
    }
  });

  server.route({
    method: 'POST',
    path: '/messages',
    handler(request, reply) {
      const db = request.mongo.db;

      db.collection('messages').insert(request.payload, function (err, result) {
        let message = result.ops[0]

        if (err) {
          return reply(Boom.internal('Internal MongoDB error', err));
        }

        async.map(message.recipients,
          (recipient) => {
            fetch(message.url, {
              method: 'POST',
              body:    JSON.stringify({
                recipient: recipient,
                body: message.body
              }),
              headers: { 'Content-Type': 'application/json' },
            })
              .then(json => console.log(json));
          },
          (err, result) => {
              if (err) {
                console.log(err)
              }
              console.log(result);
          });

        reply(message);
      });
    },
    config: {
      validate: {
        payload: {
          recipients: Joi.array().items(Joi.number().required()).required(),
          subject: Joi.string().min(3).max(64).required(),
          body: Joi.string().min(3).max(1024).required(),
          url: Joi.string().min(3).max(512).required()
        }
      }
    }
  });

  server.start(err => {
    if (err) {
      debug('unable to start server', err);
      throw err
    }

    debug('webserver started', {
      protocol: server.info.protocol,
      address: server.info.address,
      port: runtime.config.port
    })
  })
});

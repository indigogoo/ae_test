import React, {Component} from 'react';
import axios from 'axios';
import {FormControl, FormGroup, ControlLabel, Button, Form, Grid, Row, Col} from 'react-bootstrap';

import Message from './message';

export default class myForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      recipients: [
        {value: 1, label: "John"},
        {value: 2, label: "Dirk"},
        {value: 3, label: "Helen"},
        {value: 4, label: "Baster"},
        {value: 5, label: "Joe"}
      ],
      formData: {
        recipients: [],
        subject: '',
        body: '',
        url: ''
      },
      messages: []
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.handleMultipleSelectChange = this.handleMultipleSelectChange.bind(this);
  }

  handleInputChange(field, e) {
    this.setState({
      formData: Object.assign({}, this.state.formData, {[field]: e.target.value})
    });
  }

  handleMultipleSelectChange(e) {
    let selectedOptions = Array.prototype.slice.call(e.target.selectedOptions);
    let recipientIds = selectedOptions.map((option) => option.value);
    this.setState({
      formData: Object.assign({}, this.state.formData, {recipients: recipientIds})
    })
  }

  onSubmit() {
    axios.post('/messages', this.state.formData)
      .then((response) => {
        this.setState({messages: [].concat(response.data._id, this.state.messages)});
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <Row className="show-grid">
        <Col xs={6} xsOffset={3}>

          <Form>
            <FormGroup controlId="formControlsText">
              <ControlLabel>Url</ControlLabel>
              <FormControl
                type="text"
                placeholder="Enter url http[s]://domain (required)"
                onChange={this.handleInputChange.bind(this, 'url')}
              />
            </FormGroup>

            <FormGroup controlId="formControlsSelectMultiple">
              <ControlLabel>Recipients (required)</ControlLabel>
              <FormControl componentClass="select" multiple onChange={this.handleMultipleSelectChange}>
                {
                  this.state.recipients.map((recipient) => {
                    return <option key={recipient.value} value={recipient.value}>{recipient.label}</option>
                  })
                }
              </FormControl>
            </FormGroup>

            <FormGroup controlId="formControlsText">
              <ControlLabel>Subject</ControlLabel>
              <FormControl
                type="text"
                placeholder="Enter subject (required)"
                onChange={this.handleInputChange.bind(this, 'subject')}
              />
            </FormGroup>

            <FormGroup controlId="formControlsTextarea">
              <ControlLabel>Body</ControlLabel>
              <FormControl
                componentClass="textarea"
                placeholder="Message body (required)"
                onChange={this.handleInputChange.bind(this, 'body')}
              />
            </FormGroup>


            <Button onClick={this.onSubmit}>
              Submit
            </Button>
          </Form>

          { this.state.messages.map((id) => <Message key={id} messageId={id} />) }

        </Col>
      </Row>
    );
  }
}

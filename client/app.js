// const React = require('react');
// const ReactDOM = require('react-dom');
import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Form from './components/form';

class App extends React.Component {

  constructor (props) {
    super(props)
	}

  render () {
    return (
      <div>
        <Form />
      </div>
    )
  }
}

ReactDOM.render(React.createElement(App), document.getElementById('app'));

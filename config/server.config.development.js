module.exports = {
  port: process.env.PORT || 3000,
  dbOpts: {
    url: 'mongodb://mongo:27017/messages',
    settings: {
      db: {
        native_parser: false
      }
    },
    decorate: true
  }
};
